# Admin Body Class Supplement

This is a WordPress plugin that adds additional classes to the admin's body tag. It uses the the [`admin_body_class`](https://developer.wordpress.org/reference/hooks/admin_body_class/) hook to do that.

While extending the [`body_class`](https://codex.wordpress.org/Plugin_API/Filter_Reference/body_class) is [more](http://www.nathanrice.net/blog/wordpress-2-8-and-the-body_class-function/) [common](http://www.wpbeginner.com/wp-themes/wordpress-body-class-101-tips-and-tricks-for-theme-designers/), extending the admin's body tag is as [useful](https://deluxeblogtips.com/wordpress-admin-body-class/) if you wish to [customize](https://austin.passy.co/2010/extending-the-wordpress-admin_body_class/) the WordPress Admin.

## Classes

The classes are prefixed with `abcs-`, this way probably it won't conflict with other plugins.

### User

#### abcs-user-ID

Where "ID" is the ID of the logged in user.

For example: `abcs-user-164`

### Page

#### abcs-page-on-front

If the page is the [front page](https://codex.wordpress.org/Creating_a_Static_Front_Page#WordPress_Static_Front_Page_Process).

#### abcs-page-for-posts

If the page is set the [posts page](https://codex.wordpress.org/Creating_a_Static_Front_Page).

#### abcs-template-default

If the page does not have any template set.

#### abcs-template-TEMPLATE

Where "TEMPLATE" stands for the page template file name.

For example: `abcs-template-templates-hotel-partners` when the template is located in `templates` directory and the file is named `hotel-partners`.