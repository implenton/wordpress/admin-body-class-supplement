<?php
/*
 * Plugin Name: Admin Body Class Supplement
 * Plugin URI:  https://gitlab.com/implenton/wordpress/admin-body-class-supplement
 * Description: Appends some useful classes to the admin post screen
 * Version:     0.8.0
 * Author:      implenton
 * Author URI:  https://implenton.com
 * License:     GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.txt
 * GitLab Plugin URI: implenton/wordpress/admin-body-class-supplement
 */

namespace AdminBodyClassSupplement;

if ( ! defined( 'WPINC' ) ) {
    die;
}

add_filter( 'admin_body_class', __NAMESPACE__ . '\add_classes' );

function add_classes( $classes ) {

    $screen = get_current_screen();

    if ( 'post' !== $screen->base ) {
        return $classes;
    }

    $classes .= get_classes();

    return $classes;

}

function get_classes() {

    $classes   = [];
    $classes[] = get_user_id_class();
    $classes[] = get_user_role_class();
    $classes[] = get_page_front_posts_class();
    $classes[] = get_post_template_class();

    $classes = flatten_classes( $classes );
    $classes = clean_classes( $classes );
    $classes = prefix_classes( $classes );

    return \implode( ' ', $classes );

}

function get_user_id_class() {

    $current_user    = wp_get_current_user();
    $current_user_id = $current_user->ID;

    return "user-id-$current_user_id";

}

function get_user_role_class() {

    $current_user = wp_get_current_user();
    $roles        = $current_user->roles;

    return \array_map( function ( $role ) {
        return "user-role-$role";
    }, $roles );

}

function get_page_front_posts_class() {

    global $post;

    if ( get_option( 'page_on_front' ) === $post->ID ) {
        return 'page-on-front';
    }

    if ( get_option( 'page_for_posts' ) === $post->ID ) {
        return 'page-for-posts';
    }

    return '';
}

function get_post_template_class() {

    $template_slug = get_page_template_slug();

    if ( ! $template_slug ) {
        return 'template-default';
    }

    $template_slug = str_replace( '.php', '', $template_slug );
    $template_slug = str_replace( '/', '-', $template_slug );

    return "template-$template_slug";

}

function flatten_classes( $classes ) {

    $classes = new \RecursiveIteratorIterator( new \RecursiveArrayIterator( $classes ) );

    return \iterator_to_array( $classes, false );

}

function clean_classes( $classes ) {

    return \array_filter( $classes, function ( $class ) {
        return ! empty( $class );
    } );

}

function prefix_classes( $classes ) {

    return \array_map( function ( $class ) {
        return "abcs-$class";
    }, $classes );

}